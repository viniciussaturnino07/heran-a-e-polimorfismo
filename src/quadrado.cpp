#include "quadrado.hpp"

Quadrado::Quadrado(){
  tipo = "Quadrado";
  base = 12.0;
  altura = 12.0;
}

Quadrado::Quadrado(float base, float altura){
  tipo = "Quadrado";
  this->base = base;
  this->altura = altura;
}
