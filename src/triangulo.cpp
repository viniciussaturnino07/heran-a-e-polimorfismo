#include "triangulo.hpp"
#include <math.h>

Triangulo::Triangulo(){
  set_tipo("Triangulo");
  set_base(3.0);
  set_altura(4.0);
}

Triangulo::Triangulo(float base, float altura){
  set_tipo("Triangulo");
  set_base(base);
  set_altura(altura);
}

float Triangulo::calcula_area(){
  return get_base()*get_altura()/2.0;
}

float Triangulo::calcula_perimetro(){
  float lado = pow(get_base(),2) + pow(get_altura(),2);;
  lado = sqrt(lado);
  return get_base() + get_altura() + lado;
}
