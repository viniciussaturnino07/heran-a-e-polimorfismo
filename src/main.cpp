#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include "paralelogramo.hpp"

#include <iostream>

using namespace std;

int main(int argc, char const *argv[]){
  Triangulo *forma1 = new Triangulo(3.0, 4.0);
	Quadrado *forma2 = new Quadrado(12.0, 12.0);
  Circulo *forma3 = new Circulo(10.0);
  Pentagono *forma4 = new Pentagono(15.0,15.0);
  Hexagono *forma5 = new Hexagono(15.0,15.0);
  Paralelogramo *forma6 = new Paralelogramo(15.0,15.0,15.0);

  FormaGeometrica *lista[6];

  lista[0] = forma1;
  lista[1] = forma2;
  lista[2] = forma3;
  lista[3] = forma4;
  lista[4] = forma5;
  lista[5] = forma6;

  for(int i=0; i<6; i++){
    std::cout << "Forma: "<< lista[i]->get_tipo() << endl;
    std::cout << "Área: "<< lista[i]->calcula_area() << endl;
    std::cout << "Perímetro: "<< lista[i]->calcula_perimetro() << endl;
  }

  return 0;
}
