#include "circulo.hpp"
#include <math.h>

void Circulo::set_raio(float raio){
  this->raio = raio;
}

float Circulo::get_raio(){
  return this->raio;
}

Circulo::Circulo(){
  set_tipo("Circulo");
  set_raio(2.0);
}

Circulo::Circulo(float raio){
  set_tipo("Circulo");
  set_raio(raio);
}

float Circulo::calcula_area(){
  return M_PI*pow(raio,2);
}

float Circulo::calcula_perimetro(){
  return 2*M_PI*get_raio();
}
