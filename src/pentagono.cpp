#include "pentagono.hpp"

Pentagono::Pentagono(){
  set_tipo("Pentagono");
  set_base(15.0);
  set_altura(15.0);
}

Pentagono::Pentagono(float base, float altura){
  set_tipo("Pentagono");
  set_base(base);
  set_altura(altura);
}

float Pentagono::calcula_area(){
  return 5 * (get_base()*get_altura()/2.0);
}

float Pentagono::calcula_perimetro(){
  return 5 * get_base();
}
