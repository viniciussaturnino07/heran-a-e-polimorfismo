#include "hexagono.hpp"

Hexagono::Hexagono(){
  set_tipo("Hexagono");
  set_base(15.0);
  set_altura(15.0);
}

Hexagono::Hexagono(float base, float altura){
  set_tipo("Hexagono");
  set_base(base);
  set_altura(altura);
}

float Hexagono::calcula_area(){
  return 6*(get_base()*get_altura()/2.0);
}

float Hexagono::calcula_perimetro(){
  return 6*get_base();
}
